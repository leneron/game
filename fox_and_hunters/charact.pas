unit charact;

interface

uses Types, Graphics, animate, SysUtils, Forms;

type
  TMask = array [0 .. 9999, 0 .. 499] of boolean;

  TCharacter = class
  protected
    state: string;
    position: TPoint;
    vx, vy: integer;
    charheight, charwidth: integer;
  public
    procedure iter(var bm: TBitmap); virtual; Abstract;
  end;

  TPlayer = class(TCharacter)
  private
    lifes: byte;
    lifes_image: TBitmap;
    lifes_bg: TBitmap;
    current_points: integer;
    last_pos:integer;
  public
    animation_up_left, animation_up_right, animation_run_right,
      animation_run_left, animation_stand_left, animation_stand_right,
      animation_dead: TAnimation;
    procedure set_lifes(life: shortint);
    procedure set_points(points: integer);
    procedure set_state;
    procedure move_left(var Mask: TMask);
    procedure move_right(var Mask: TMask);
    procedure fall(var Mask: TMask);
    procedure jump(var Mask: TMask);
    procedure die;
    procedure hide;
    function get_lifes: byte;
    function get_animation: TAnimation;
    function get_bg_position: integer;
    function get_x: integer;
    function get_y: integer;
    procedure iter(var bm: TBitmap); override;
    procedure information(var bm: TBitmap);
    procedure total_points(var bm: TBitmap);
    function check_dead: boolean;
    function check_hidden: boolean;
    function get_points: integer;
    function enough_points: boolean;
    constructor Create;
    procedure load;
  end;

  THunter = class(TCharacter)
  private
    function check(player_pos_x, player_pos_y: integer): boolean; virtual;
    procedure run(var Mask: TMask); virtual;
    procedure fall(var Mask: TMask);
    function get_animation: TAnimation; virtual; Abstract;
  public
    constructor Create(s: string); virtual; Abstract;
    procedure set_state(player_pos_x, player_pos_y: integer); virtual;
  end;

  TSlow = class(THunter)
  private
    function check(player_pos_x, player_pos_y: integer): boolean; override;
  public
    animation_stand_right, animation_shoot_right, animation_stand_left,
      animation_shoot_left: TAnimation;
    constructor Create(s: string); override;
    function get_animation: TAnimation; override;
  end;

  THigh = class(THunter)
  private
    function check(player_pos_x, player_pos_y: integer): boolean; override;
  public
    animation_stand_right, animation_shoot_right, animation_stand_left,
      animation_shoot_left: TAnimation;
    constructor Create(s: string); override;
    function get_animation: TAnimation; override;
  end;

  TDog = class(THunter)
  private
    animation_run_left, animation_run_right: TAnimation;
    function check(player_pos_x, player_pos_y: integer): boolean; override;
    procedure move_left(var Mask: TMask);
    procedure move_right(var Mask: TMask);
    function get_animation: TAnimation; override;
  public
    constructor Create(s: string); override;
    procedure run(var Mask: TMask); override;
  end;

  TTrap = class(THunter)
  private
    hidden_image, broken_image: TAnimation;
    function check(player_pos_x, player_pos_y: integer): boolean; override;
    function get_animation: TAnimation; override;
  public
    constructor Create(s: string); override;
  end;

  THunters_list = class
  private
    list: array of THunter;
  public
    constructor Create(number: integer);
    function check_all(player_pos_x, player_pos_y: integer): boolean;
    procedure iter(var bm: TBitmap);
    procedure move(var Mask: TMask);
  end;

const
  g = -10;
  MAXLIFES = 20;
  MINPOINTS = 30;

implementation

// Character--------------------------------------------------------------------

// Player-----------------------------------------------------------------------
constructor TPlayer.Create;
begin
  state := 'stay';
  animation_stand_right := TAnimation.Create;
  animation_stand_left := TAnimation.Create;
  animation_run_right := TAnimation.Create;
  animation_run_left := TAnimation.Create;
  animation_up_right := TAnimation.Create;
  animation_up_left := TAnimation.Create;
  animation_dead := TAnimation.Create;
  animation_stand_right.load('character\player\stand_right\');
  animation_stand_left.load('character\player\stand_left\');
  animation_run_right.load('character\player\run_right\');
  animation_run_left.load('character\player\run_left\');
  animation_up_right.load('character\player\up_right\');
  animation_up_left.load('character\player\up_left\');
  animation_dead.load('character\player\dead\');
  lifes_image := TBitmap.Create;
  lifes_image.LoadFromFile('character\player\lifes.bmp');
  lifes_image.Transparent := true;
  lifes_image.TransparentColor := $FF00FF;
  lifes_bg := TBitmap.Create;
  lifes_bg.LoadFromFile('character\player\lifes_bg.bmp');
  lifes_bg.Transparent := true;
  lifes_bg.TransparentColor := $FF00FF;
  charwidth := get_animation.current_frame.width div 2;
  // ������ �������� ���������
  charheight := get_animation.current_frame.height div 2;
  // ������ �������� ���������
end;

procedure TPlayer.load;
begin
  state := 'stand_right';
  position.X := 100;
  position.Y := 300;
  last_pos:=0;
  vx := 0;
  vy := 0;
  lifes := MAXLIFES;
  current_points := 0;
end;

function TPlayer.get_x: integer;
begin
  get_x := position.X;
end;

function TPlayer.get_y: integer;
begin
  get_y := position.Y;
end;

procedure TPlayer.set_state;
begin
  if (not(state = 'dead')) then
  begin
    if (state = 'run_left') or (state = 'stand_left') or (state = 'up_left')
      then
      state := 'stand_left'
    else
      state := 'stand_right';
  end;
end;

procedure TPlayer.move_left(var Mask: TMask);
var
  tempx: integer;
begin
  state := 'run_left';
  vx := -20;
  tempx := position.X + vx;
  while (position.X >= 100) and (position.X >= tempx) and
    (Mask[position.X - charwidth, 500 - position.Y] = true) do
    dec(position.X);
  if (Mask[position.X - charwidth, (500 + charheight) - position.Y] = false)
    and (Mask[position.X - charwidth, 500 - position.Y] = true) then
  begin
    while (Mask[position.X - charwidth, (500 + charheight) - position.Y]
        = false) do
      inc(position.Y);
  end;
end;

procedure TPlayer.move_right(var Mask: TMask);
var
  tempx: integer;
begin
  state := 'run_right';
  vx := 20;
  tempx := position.X + vx;
  while (position.X <= 4900) and (position.X <= tempx) and
    (Mask[position.X + charwidth, 500 - position.Y] = true) do
    inc(position.X);
  if (Mask[position.X + charwidth, (500 + charheight) - position.Y] = false)
    and (Mask[position.X + charwidth, 500 - position.Y] = true) then
  begin
    while (Mask[position.X + charwidth, (500 + charheight) - position.Y]
        = false) do
      inc(position.Y);
  end;
end;

procedure TPlayer.jump(var Mask: TMask);
var
  tempy: integer;
begin
  if (state = 'run_left') or (state = 'stand_left') then
    state := 'up_left'
  else
    state := 'up_right';
  tempy := position.Y + 35;
  while (position.Y >= charheight) and (position.Y <= tempy)
  and (Mask[position.X, (500 - charheight) - position.Y] = true) do
    inc(position.Y);
end;

procedure TPlayer.fall(var Mask: TMask);
var
  tempy: integer;
begin
  vy := vy + g;
  tempy := position.Y + vy;
  while (position.Y >= charheight) and (position.Y >= tempy)
    and (Mask[position.X, (500 + charheight) - position.Y] = true) do
    dec(position.Y);
  if (Mask[position.X, (500 + charheight) - position.Y] = false) then
    vy := 0;
end;

procedure TPlayer.die;
begin
  state := 'dead';
end;

procedure TPlayer.hide;
begin
  state := 'hidden';
end;

function TPlayer.check_dead: boolean;
begin
  if state = 'dead' then
    check_dead := true
  else
    check_dead := false;
end;

function TPlayer.check_hidden: boolean;
begin
  if state = 'hidden' then
    check_hidden := true
  else
    check_hidden := false;
end;

function TPlayer.enough_points: boolean;
begin
  if current_points >= MINPOINTS then
    enough_points := true
  else
    enough_points := false;
end;

// ���������� ������ ��� ��������� ��������
function TPlayer.get_animation: TAnimation;
begin
  if state = 'dead' then
    get_animation := animation_dead
  else if state = 'stand_right' then
    get_animation := animation_stand_right
  else if state = 'stand_left' then
    get_animation := animation_stand_left
  else if state = 'run_right' then
    get_animation := animation_run_right
  else if state = 'run_left' then
    get_animation := animation_run_left
  else if state = 'up_right' then
    get_animation := animation_up_right
  else if state = 'up_left' then
    get_animation := animation_up_left
  else
    get_animation := animation_stand_right;
end;

// ���������� ������� ����
function TPlayer.get_bg_position: integer;
var
  pos_bg: integer;
begin
  if ((position.X < 4700) and (position.X > 400)) then
    pos_bg := -position.X + 400
  else if (position.X <= 400) then
    pos_bg := 0
  else if (position.X >= 4700) then
    pos_bg := -4300;
  if (pos_bg<0) and (pos_bg>-4300) then begin
	if (pos_bg >= last_pos+30)then
    pos_bg := last_pos+30;
	if (pos_bg <= last_pos-30) then
    pos_bg := last_pos-30;
  end;	
  last_pos := pos_bg;
  get_bg_position := pos_bg;
end;

procedure TPlayer.set_points(points: integer);
begin
  current_points := current_points + points;
end;

procedure TPlayer.set_lifes(life: shortint);
begin
  lifes := lifes + life;
end;

function TPlayer.get_lifes: byte;
begin
  get_lifes := lifes;
end;

procedure TPlayer.iter(var bm: TBitmap);
var
  frame: TBitmap;
begin
  frame := get_animation.current_frame;
  bm.Canvas.Draw(position.X - charwidth, (500 - charheight) - position.Y,
    frame);
end;

procedure TPlayer.information(var bm: TBitmap);
var
  i, percents: byte;
  X, Y: integer;
  s: string;
begin
  X := 10;
  for i := 1 to lifes do
  begin
    bm.Canvas.Draw(X, 10, lifes_image);
    X := X + lifes_image.width;
  end;
  bm.Canvas.Brush.Style := bsClear;
  bm.Canvas.Font.Color := clWhite;
  percents := round(lifes / MAXLIFES * 100);
  s := '��������: ' + inttostr(percents) + '%';
  Y := (lifes_image.height - 10) div 2 + 10;
  bm.Canvas.TextOut(15, Y - 2, s);
  bm.Canvas.Draw(9, 9, lifes_bg);
  bm.Canvas.Font.Size := 10;
  s := '����: ' + inttostr(current_points);
  X := 690 - bm.Canvas.TextWidth(s);
  bm.Canvas.TextOut(X, Y - 2, s);
end;

function TPlayer.get_points: integer;
begin
  get_points := current_points;
end;

procedure TPlayer.total_points(var bm: TBitmap);
var
  s: string;
  X, Y: integer;
  enrolled_points: integer;
begin
  bm.Canvas.Brush.Style := bsClear;
  bm.Canvas.Font.Color := clWhite;
  bm.Canvas.Font.Size := 20;
  if (check_dead) then
    s := '������� �� ������� :('
  else if (not enough_points) then
    s := '������������ �����'
  else
    s := '������� �������';
  X := (700 - bm.Canvas.TextWidth(s)) div 2;
  bm.Canvas.TextOut(X, 200, s);
  if (check_dead) then
    current_points := 0;
  bm.Canvas.Font.Size := 16;
  s := '������� ������: ' + inttostr(current_points);
  X := (700 - bm.Canvas.TextWidth(s)) div 2;
  bm.Canvas.TextOut(X, 235, s);
end;

// Hunter-----------------------------------------------------------------------

function THunter.check(player_pos_x, player_pos_y: integer): boolean;
begin
  check := false;
end;

procedure THunter.set_state(player_pos_x: integer; player_pos_y: integer);
begin
  if player_pos_x > position.X then
    state := state + '_right'
  else
    state := state + '_left';
end;

procedure THunter.run(var Mask: TMask);
begin
end;

procedure THunter.fall(var Mask: TMask);
var
  tempy: integer;
begin
  vy := vy + g;
  tempy := position.Y + vy;
  while (position.Y >= charheight) and (position.Y >= tempy)
  and (Mask[position.X, (500 + charheight) - position.Y] = true) do
    dec(position.Y);
  if (Mask[position.X, (500 + charheight) - position.Y] = false) then
    vy := 0;
end;

// Slow-------------------------------------------------------------------------

constructor TSlow.Create(s: string);
begin
  animation_stand_left := TAnimation.Create;
  animation_stand_left.load(s + 'stand_left\');
  animation_stand_right := TAnimation.Create;
  animation_stand_right.load(s + 'stand_right\');
  animation_shoot_left := TAnimation.Create;
  animation_shoot_left.load(s + 'shoot_left\');
  animation_shoot_right := TAnimation.Create;
  animation_shoot_right.load(s + 'shoot_right\');
  charwidth := get_animation.current_frame.width div 2;
  charheight := get_animation.current_frame.height div 2;
end;

function TSlow.get_animation: TAnimation;
begin
  if state = 'stand_right' then
    get_animation := animation_stand_right
  else if state = 'shoot_right' then
    get_animation := animation_shoot_right
  else if state = 'stand_left' then
    get_animation := animation_stand_left
  else if state = 'shoot_left' then
    get_animation := animation_shoot_left
  else
    get_animation := animation_stand_right;
end;

function TSlow.check(player_pos_x, player_pos_y: integer): boolean;
var
  r: integer;
begin
  check := false;
  r := round(sqrt(sqr(position.X - player_pos_x) + sqr
        (position.Y - player_pos_y)));
  if (r < 150) and (player_pos_y - position.Y < 100) and
    (position.Y - player_pos_y > -40) then
  begin
    state := 'shoot';
    set_state(player_pos_x, player_pos_y);
    check := true;
  end
  else
  begin
    state := 'stand';
    set_state(player_pos_x, player_pos_y);
  end;
end;

// High-------------------------------------------------------------------------

constructor THigh.Create(s: string);
begin
  animation_stand_left := TAnimation.Create;
  animation_stand_left.load(s + 'stand_left\');
  animation_stand_right := TAnimation.Create;
  animation_stand_right.load(s + 'stand_right\');
  animation_shoot_left := TAnimation.Create;
  animation_shoot_left.load(s + 'shoot_left\');
  animation_shoot_right := TAnimation.Create;
  animation_shoot_right.load(s + 'shoot_right\');
  charwidth := get_animation.current_frame.width div 2;
  charheight := get_animation.current_frame.height div 2;
end;

function THigh.get_animation: TAnimation;
begin
  if state = 'stand_right' then
    get_animation := animation_stand_right
  else if state = 'shoot_right' then
    get_animation := animation_shoot_right
  else if state = 'stand_left' then
    get_animation := animation_stand_left
  else if state = 'shoot_left' then
    get_animation := animation_shoot_left
  else
    get_animation := animation_stand_right;
end;

function THigh.check(player_pos_x, player_pos_y: integer): boolean;
var
  r: integer;
begin
  check := false;
  r := round(sqrt(sqr(position.X - player_pos_x) + sqr
        (position.Y - player_pos_y)));
  if (r < 50) and (player_pos_y - position.Y < 150) and
    (position.Y - player_pos_y > -20) then
  begin
    state := 'shoot';
    set_state(player_pos_x, player_pos_y);
    check := true;
  end
  else
  begin
    state := 'stand';
    set_state(player_pos_x, player_pos_y);
  end;
end;

// Dog--------------------------------------------------------------------------

constructor TDog.Create(s: string);
begin
  animation_run_left := TAnimation.Create;
  animation_run_left.load(s + 'run_left\');
  animation_run_right := TAnimation.Create;
  animation_run_right.load(s + 'run_right\');
  charwidth := get_animation.current_frame.width div 2;
  charheight := get_animation.current_frame.height div 2;
end;

function TDog.get_animation: TAnimation;
begin
  if state = 'run_left' then
    get_animation := animation_run_left
  else if state = 'run_right' then
    get_animation := animation_run_right
  else
    get_animation := animation_run_right;
end;

function TDog.check(player_pos_x, player_pos_y: integer): boolean;
begin
  if ((abs(player_pos_x - position.X) < 40) and (abs(player_pos_y - position.Y)
        < 25)) then
    check := true
  else
    check := false;
end;

procedure TDog.move_left(var Mask: TMask);
var
  tempx: integer;
begin
  state := 'run_left';
  vx := -20;
  tempx := position.X + vx;
  while (position.X >= 100) and (position.X >= tempx) and
    (Mask[position.X - charwidth, 500 - position.Y] = true) do
    dec(position.X);
  if (Mask[position.X - charwidth, (500 + charheight) - position.Y] = false)
    and (Mask[position.X - charwidth, 500 - position.Y] = true) then
  begin
    while (Mask[position.X - charwidth, (500 + charheight) - position.Y]
        = false) do
      inc(position.Y);
  end;
  if (Mask[position.X - charwidth - 40, 500 - position.Y] = false) then
    state := 'run_right'
  else
    state := 'run_left';
end;

procedure TDog.move_right(var Mask: TMask);
var
  tempx: integer;
begin
  vx := 20;
  tempx := position.X + vx;
  while (position.X <= 4900) and (position.X <= tempx) and
    (Mask[position.X + charwidth, 500 - position.Y] = true) do
    inc(position.X);
  if (Mask[position.X + charwidth, (500 + charheight) - position.Y] = false)
    and (Mask[position.X + charwidth, 500 - position.Y] = true) then
  begin
    while (Mask[position.X + charwidth, (500 + charheight) - position.Y]
        = false) do
      inc(position.Y);
  end;
  if (Mask[position.X + charwidth + 40, 500 - position.Y] = false) then
    state := 'run_left'
  else
    state := 'run_right';
end;

procedure TDog.run(var Mask: TMask);
begin
  if state = 'run_left' then
    move_left(Mask)
  else
    move_right(Mask);
end;

// Trap--------------------------------------------------------------------------

constructor TTrap.Create(s: string);
begin
  hidden_image := TAnimation.Create;
  hidden_image.load(s + 'hidden\');
  broken_image := TAnimation.Create;
  broken_image.load(s + 'broken\');
  charwidth := get_animation.current_frame.width div 2;
  charheight := get_animation.current_frame.height div 2;
  state := 'hidden';
end;

function TTrap.check(player_pos_x, player_pos_y: integer): boolean;
begin
  if ((abs(player_pos_x - position.X) < charwidth) and
      (abs(player_pos_y - position.Y) < charheight)) then
  begin
    check := true;
    state := 'broken';
  end
  else
    check := false;
end;

function TTrap.get_animation: TAnimation;
begin
  if state = 'broken' then
    get_animation := broken_image
  else
    get_animation := hidden_image;
end;

// Hunters_list-----------------------------------------------------------------

constructor THunters_list.Create(number: integer);
var
  s: string;
  i, Size: integer;
  f: textfile;
begin
  // �������� ���������
  s := 'character\hunters\hunters\' + inttostr(number) + '\';

  assignfile(f, s + 'slow.txt');
  reset(f);
  while (not seekeof(f)) do
  begin
    Size := Length(list);
    setLength(list, Size + 1);
    list[Size] := TSlow.Create('character\hunters\hunters\slow\');
    read(f, list[Size].position.X);
    read(f, list[Size].position.Y);
    list[Size].vy := 0;
  end;
  closefile(f);

  assignfile(f, s + 'high.txt');
  reset(f);
  while (not seekeof(f)) do
  begin
    Size := Length(list);
    setLength(list, Size + 1);
    list[Size] := THigh.Create('character\hunters\hunters\high\');
    read(f, list[Size].position.X);
    read(f, list[Size].position.Y);
    list[Size].vy := 0;
  end;
  closefile(f);

  assignfile(f, s + 'dog.txt');
  reset(f);
  while (not seekeof(f)) do
  begin
    Size := Length(list);
    setLength(list, Size + 1);
    list[Size] := TDog.Create('character\hunters\hunters\dog\');
    read(f, list[Size].position.X);
    read(f, list[Size].position.Y);
    list[Size].vy := 0;
  end;
  closefile(f);

  // �������� �������
  s := 'character\hunters\traps\';

  assignfile(f, s + 'kind1\' + inttostr(number) + '\trap.txt');
  reset(f);
  while (not seekeof(f)) do
  begin
    Size := Length(list);
    setLength(list, Size + 1);
    list[Size] := TTrap.Create('character\hunters\traps\kind1\');
    read(f, list[Size].position.X);
    read(f, list[Size].position.Y);
  end;
  closefile(f);

  assignfile(f, s + 'kind2\' + inttostr(number) + '\trap.txt');
  reset(f);
  while (not seekeof(f)) do
  begin
    Size := Length(list);
    setLength(list, Size + 1);
    list[Size] := TTrap.Create('character\hunters\traps\kind2\');
    read(f, list[Size].position.X);
    read(f, list[Size].position.Y);
  end;
  closefile(f);

  assignfile(f, s + 'kind3\' + inttostr(number) + '\trap.txt');
  reset(f);
  while (not seekeof(f)) do
  begin
    Size := Length(list);
    setLength(list, Size + 1);
    list[Size] := TTrap.Create('character\hunters\traps\kind3\');
    read(f, list[Size].position.X);
    read(f, list[Size].position.Y);
  end;
  closefile(f);

end;

function THunters_list.check_all(player_pos_x, player_pos_y: integer): boolean;
var
  i: integer;
  flag: boolean;
begin
  if (Length(list) >= 0) then
    i := 0;
  flag := false;
  while (i < Length(list)) and (Length(list) > 0) and (flag = false) do
  begin
    if list[i].check(player_pos_x, player_pos_y) then
      flag := true
    else
      inc(i);
  end;
  if flag = true then
    check_all := true
  else
    check_all := false;
end;

procedure THunters_list.iter(var bm: TBitmap);
var
  i, huntheight, huntwidth: integer;
  frame: TBitmap;
begin
  if (Length(list) > 0) then
    for i := 0 to (Length(list) - 1) do
    begin
      frame := list[i].get_animation.current_frame;
      bm.Canvas.Draw(list[i].position.X - list[i].charwidth,
        (500 - list[i].charheight) - list[i].position.Y, frame);
    end;
end;

procedure THunters_list.move(var Mask: TMask);
var
  i: integer;
begin
  if (Length(list) > 0) then
    for i := 0 to (Length(list) - 1) do
    begin
      list[i].run(Mask);
      list[i].fall(Mask);
    end;
end;

end.
