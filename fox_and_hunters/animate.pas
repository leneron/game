unit animate;

interface

uses SysUtils, Graphics;

type  TAnimation = class
      private
        Frames: array of TBitmap;
        current: byte;
      public
      procedure iter;
        procedure load(s: string);
        function current_frame: TBitmap;
    end;

implementation

  procedure Tanimation.load(s: string);
  var r: TSearchRec; size: integer;
  begin
    if FindFirst(s+'*.bmp', faAnyFile, r)=0 then
    begin
      repeat
        size:=Length(Frames);
        setLength(Frames, size+1);
        Frames[size]:=TBitmap.Create;
        Frames[size].LoadFromFile(s+r.Name);
        Frames[size].TransparentColor:=$ff00ff;
        Frames[size].Transparent:=true;
      until FindNext(r)<>0;
      FindClose(r);
    end;
    current:=0;
  end;

  procedure Tanimation.iter;
  var len: integer;
  begin
    len:=Length(Frames);
    current:=(current+1) mod (len+1);
  end;

  function Tanimation.current_frame: TBitmap;
  begin
    current_frame:=Frames[current div 2];
    iter;
  end;

end.
