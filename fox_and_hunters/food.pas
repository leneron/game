unit food;

interface

uses Types, Graphics, SysUtils, charact, animate;

type
  TFood = class
    private
      position: TPoint;
    public
      food_type: string;
      food_image: TAnimation;
      procedure load(s: string);
      function check_ate(player_pos_x, player_pos_y: integer): boolean;
  end;

  TFood_list = class
    private
      list: array of TFood;
    public
      procedure iter(var bm: TBitmap);
      procedure del(number: integer);
      function check(player_pos_x, player_pos_y: integer):integer;
      constructor create(level_number: integer);
  end;

implementation

//Food--------------------------------------------------------------------------

  procedure TFood.load(s: string);
  begin
    food_image:=TAnimation.Create;
    food_image.load(s);
  end;

  function TFood.check_ate(player_pos_x, player_pos_y: integer):boolean;
  begin
    if (abs(player_pos_x-position.X)<20) and (abs(player_pos_y-position.Y)<20) then check_ate:=true
    else check_ate:=false;
  end;

//Food_list---------------------------------------------------------------------

  constructor TFood_list.create(level_number: integer);
  var s: string; f: textfile; size: integer;
  begin
    s:='food\'+inttostr(level_number)+'\';
    //������ ����������
    assignfile(f, s+'mice.txt');
    reset(f);
    while (not seekeof(f)) do begin
          size:=Length(list);
          setLength(list, size+1);
          list[size]:=TFood.Create;
          list[size].food_type:='mice';
          list[size].load('food\mice\');
          read(f, list[size].position.X);
          read(f, list[size].position.Y);
    end;
    closefile(f);

    assignfile(f, s+'egg.txt');
    reset(f);
    while (not seekeof(f)) do begin
          size:=Length(list);
          setLength(list, size+1);
          list[size]:=TFood.Create;
          list[size].food_type:='egg';
          list[size].load('food\egg\');
          read(f, list[size].position.X);
          read(f, list[size].position.Y);
    end;
    closefile(f);

    assignfile(f, s+'bird.txt');
    reset(f);
    while (not seekeof(f)) do begin
          size:=Length(list);
          setLength(list, size+1);
          list[size]:=TFood.Create;
          list[size].food_type:='bird';
          list[size].load('food\bird\');
          read(f, list[size].position.X);
          read(f, list[size].position.Y);
    end;
    closefile(f);
  end;

  procedure TFood_list.del(number: integer);
  var len: integer; temp: TFood;
  begin
    len:=Length(list)-1;
    //������ ������ ���������� �������� ���������
    temp:=list[len];
    list[number]:=temp;
    setLength(list, len);
  end;

  function TFood_list.check(player_pos_x, player_pos_y: integer):integer;
  var i: integer;
  begin
    check:=0;
    if (Length(list)>=0) then
    i:=0;
    while (i<Length(list)) and (Length(list)>0) do begin
      if list[i].check_ate(player_pos_x, player_pos_y)
          then begin
            if list[i].food_type='mice' then check:=10
            else if list[i].food_type='egg' then check:=5
            else if list[i].food_type='bird' then check:=-1;
            del(i);
          end
      else inc(i);
    end;
  end;

  procedure TFood_list.iter(var bm: TBitmap);
  var i: integer;
  begin
    if (Length(list)>0) then
    for i:=0 to (Length(list)-1) do begin
      bm.Canvas.Draw(list[i].position.X-20,
                    480-list[i].position.Y,
                    list[i].food_image.current_frame);
    end;
  end;

end.
