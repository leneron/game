object window: Twindow
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Fox and Hunters'
  ClientHeight = 500
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = top_menu
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object background: TPaintBox
    Left = 0
    Top = 0
    Width = 700
    Height = 500
  end
  object button_new_game: TButton
    Left = 275
    Top = 188
    Width = 145
    Height = 33
    Caption = #1053#1086#1074#1072#1103' '#1080#1075#1088#1072
    TabOrder = 0
    OnClick = button_new_gameClick
  end
  object button_continue: TButton
    Left = 275
    Top = 227
    Width = 145
    Height = 33
    Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1075#1088#1091
    TabOrder = 1
    OnClick = button_continueClick
  end
  object button_exit: TButton
    Left = 275
    Top = 266
    Width = 145
    Height = 33
    Caption = #1042#1099#1093#1086#1076
    Enabled = False
    TabOrder = 2
    OnClick = button_exitClick
  end
  object button_return: TButton
    Left = 275
    Top = 148
    Width = 145
    Height = 33
    Caption = #1042#1077#1088#1085#1091#1090#1100#1089#1103' '#1074' '#1080#1075#1088#1091
    TabOrder = 3
    Visible = False
    OnClick = button_returnClick
  end
  object Level1: TButton
    Left = 89
    Top = 192
    Width = 104
    Height = 25
    Caption = #1059#1088#1086#1074#1077#1085#1100' 1'
    TabOrder = 4
    Visible = False
    OnClick = Level1Click
  end
  object Level2: TButton
    Left = 297
    Top = 196
    Width = 104
    Height = 25
    Caption = #1059#1088#1086#1074#1077#1085#1100' 2'
    TabOrder = 5
    Visible = False
    OnClick = Level2Click
  end
  object Level3: TButton
    Left = 505
    Top = 192
    Width = 104
    Height = 25
    Caption = #1059#1088#1086#1074#1077#1085#1100' 3'
    TabOrder = 6
    Visible = False
    OnClick = Level3Click
  end
  object Level4: TButton
    Left = 89
    Top = 380
    Width = 104
    Height = 25
    Caption = #1059#1088#1086#1074#1077#1085#1100' 4'
    TabOrder = 7
    Visible = False
    OnClick = Level4Click
  end
  object Level5: TButton
    Left = 297
    Top = 380
    Width = 104
    Height = 25
    Caption = #1059#1088#1086#1074#1077#1085#1100' 5'
    TabOrder = 8
    Visible = False
    OnClick = Level5Click
  end
  object Level6: TButton
    Left = 505
    Top = 380
    Width = 104
    Height = 25
    Caption = #1059#1088#1086#1074#1077#1085#1100' 6'
    TabOrder = 9
    Visible = False
    OnClick = Level6Click
  end
  object top_menu: TMainMenu
    Left = 16
    Top = 16
    object button_menu: TMenuItem
      Caption = #1052#1077#1085#1102
      Enabled = False
      OnClick = button_menuClick
    end
    object button_control: TMenuItem
      Caption = #1050#1072#1082' '#1080#1075#1088#1072#1090#1100
      OnClick = button_controlClick
    end
  end
  object keys_timer: TTimer
    Enabled = False
    Interval = 1
    OnTimer = keys_timerTimer
    Left = 136
    Top = 16
  end
  object game_timer: TTimer
    Enabled = False
    Interval = 1
    OnTimer = game_timerTimer
    Left = 72
    Top = 16
  end
  object hunters_timer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = hunters_timerTimer
    Left = 320
    Top = 16
  end
  object hunters_moving_timer: TTimer
    Enabled = False
    Interval = 1
    OnTimer = hunters_moving_timerTimer
    Left = 224
    Top = 16
  end
  object level_over_timer: TTimer
    Enabled = False
    Interval = 50
    OnTimer = level_over_timerTimer
    Left = 416
    Top = 16
  end
end
