unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, StdCtrls, charact, level, food, animate, bg;

type

  Twindow = class(TForm)
    background: TPaintBox;
    button_new_game: TButton;
    button_continue: TButton;
    button_exit: TButton;
    button_menu: TMenuItem;
    button_control: TMenuItem;
    top_menu: TMainMenu;
    keys_timer: TTimer;
    game_timer: TTimer;
    hunters_timer: TTimer;
    button_return: TButton;
    hunters_moving_timer: TTimer;
    Level1: TButton;
    Level2: TButton;
    Level3: TButton;
    Level4: TButton;
    Level5: TButton;
    Level6: TButton;
    level_over_timer: TTimer;
    procedure button_new_gameClick(Sender: TObject);
    procedure button_continueClick(Sender: TObject);
    procedure button_exitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure button_menuClick(Sender: TObject);
    procedure keys_timerTimer(Sender: TObject);
    procedure game_timerTimer(Sender: TObject);
    procedure button_controlClick(Sender: TObject);
    procedure hunters_timerTimer(Sender: TObject);
    procedure hunters_moving_timerTimer(Sender: TObject);
    procedure button_returnClick(Sender: TObject);
    procedure Level1Click(Sender: TObject);
    procedure Level2Click(Sender: TObject);
    procedure Level3Click(Sender: TObject);
    procedure Level4Click(Sender: TObject);
    procedure Level5Click(Sender: TObject);
    procedure Level6Click(Sender: TObject);
    procedure level_over_timerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  private
    Bg: TBg;
    Player: TPlayer;
    Hunters: THunters_list;
    Level: TLevel;
    Levels_list: TLevels_list;
    Food_list: TFood_list;
    appeared_menu: boolean;
    procedure save;
    procedure load;
    procedure game_over;
    procedure new_game;
    procedure continue;
    procedure next(var bm:TBitmap);
  public
  end;

var
  window: Twindow;

implementation

{$R *.dfm}

procedure Twindow.button_continueClick(Sender: TObject);
var bm: TBitmap;
begin
  button_menu.Enabled:=true;
  appeared_menu:=true;
  button_return.Visible := false;
  button_new_game.Visible := false;
  button_continue.Visible := false;
  button_exit.Visible := false;
  bm:=TBitmap.Create;
  bm.Height:=500;
  bm.Width:=700;
  bm.Canvas.Draw(0, 0, levels_list.MenuBg);
  Levels_list.display(bm);
  background.Canvas.draw(0,0,bm);
  bm.Destroy;
  Level1.Visible:=true;
  Level2.Visible:=true;
  Level3.Visible:=true;
  Level4.Visible:=true;
  Level5.Visible:=true;
  Level6.Visible:=true;
end;

procedure Twindow.button_controlClick(Sender: TObject);
begin
  button_menu.Enabled:=true;
  appeared_menu:=true;
  button_return.Visible := false;
  button_new_game.Visible := false;
  button_continue.Visible := false;
  button_exit.Visible := false;
  level_over_timer.Enabled:=false;
  Level1.Visible:=false;
  Level2.Visible:=false;
  Level3.Visible:=false;
  Level4.Visible:=false;
  Level5.Visible:=false;
  Level6.Visible:=false;
  keys_timer.Enabled := false;
  hunters_timer.Enabled := false;
  hunters_moving_timer.Enabled := false;
  game_timer.Enabled := false;
  level_over_timer.Enabled:=false;
  Canvas.Draw(0, 0, levels_list.ControlBg);
end;

procedure Twindow.button_exitClick(Sender: TObject);
begin
  game_over;
  window.Close;
end;

procedure Twindow.button_menuClick(Sender: TObject);
begin
  button_menu.Enabled:=false;
  button_exit.Enabled:=true;
  keys_timer.Enabled := false;
  hunters_timer.Enabled := false;
  hunters_moving_timer.Enabled := false;
  game_timer.Enabled := false;
  level_over_timer.Enabled:=false;
  Level1.Visible:=false;
  Level2.Visible:=false;
  Level3.Visible:=false;
  Level4.Visible:=false;
  Level5.Visible:=false;
  Level6.Visible:=false;
  background.Canvas.Draw(0, 0, levels_list.MenuBg);
  button_return.Visible := true;
  button_new_game.Visible := true;
  button_continue.Visible := true;
  button_exit.Visible := true;
end;

procedure Twindow.button_new_gameClick(Sender: TObject);
begin
  appeared_menu:=true;
  button_menu.Enabled:=true;
  button_return.Visible := false;
  button_new_game.Visible := false;
  button_continue.Visible := false;
  button_exit.Visible := false;
  background.Canvas.Draw(0, 0, levels_list.LoadBg);
  new_game;
  load;
end;

procedure Twindow.button_returnClick(Sender: TObject);
begin
  button_menu.Enabled:=true;
  button_return.Visible := false;
  button_new_game.Visible := false;
  button_continue.Visible := false;
  button_exit.Visible := false;
  background.Canvas.Draw(0, 0, levels_list.MenuBg);
  if Player.check_dead then
    continue;
  Level1.Visible:=false;
  Level2.Visible:=false;
  Level3.Visible:=false;
  Level4.Visible:=false;
  Level5.Visible:=false;
  Level6.Visible:=false;
  keys_timer.Enabled := true;
  hunters_timer.Enabled := true;
  hunters_moving_timer.Enabled := true;
  game_timer.Enabled := true;
  level_over_timer.Enabled:=true;
end;

procedure Twindow.FormCreate(Sender: TObject);
var n: integer;
begin
  Bg := TBg.Create;
  Levels_list:=TLevels_list.Create;
  Player := TPlayer.Create;
  n:=1;
  Level := TLevel.Create(n);
  Food_list := TFood_list.Create(n);
  Bg.load(n);
  appeared_menu:=false;
end;

procedure Twindow.FormDestroy(Sender: TObject);
begin
  Bg.Destroy;
  Player.Destroy;
  Levels_list.Destroy;
  Food_list.Destroy;
end;

procedure Twindow.FormPaint(Sender: TObject);
begin
  if appeared_menu=false then background.Canvas.Draw(0, 0, levels_list.MenuBg);
end;

procedure Twindow.game_timerTimer(Sender: TObject);
var
  bm: TBitmap;
  pos_bg: integer;
  x,y:integer;
begin
  bm := TBitmap.Create;
  bm.Height := level.levelheight;
  bm.Width := level.levelwidth;
  Bg.iter(bm);
  Food_list.iter(bm);
  Hunters.iter(bm);
  Player.iter(bm);
  pos_bg := Player.get_bg_position;
  bm.Canvas.Draw(pos_bg, 0, bm);
  if (not Player.check_dead) and (not (Player.check_hidden)) then next(bm);
  if (Player.check_dead) or (Player.check_hidden) then player.total_points(bm);
  background.Canvas.Draw(0, 0, bm);
  bm.Destroy;
end;

procedure Twindow.hunters_moving_timerTimer(Sender: TObject);
begin
  Hunters.move(level.Mask);
end;

procedure Twindow.hunters_timerTimer(Sender: TObject);
var
  x, y: integer;
begin
  x := Player.get_x;
  y := Player.get_y;
  if (Player.get_lifes > 0) then
  begin
    if Hunters.check_all(x, y) then
      Player.set_lifes(-1)
  end
  else
  begin
    keys_timer.Enabled := false;
    if (not Player.check_dead) then
    begin
      Player.die;
      Application.ProcessMessages;
    end;
    sleep(3000);
    button_menu.Click;
  end;
end;

procedure Twindow.keys_timerTimer(Sender: TObject);
var
  x, y, points: integer;
begin
  Player.set_state;
  if (Word(GetKeyState(vk_up)) shr 8) > 0 then
    Player.jump(level.Mask);
  if (Word(GetKeyState(vk_left)) shr 8) > 0 then
    Player.move_left(level.Mask);
  if (Word(GetKeyState(vk_right)) shr 8) > 0 then
    Player.move_right(level.Mask);
  Player.fall(level.Mask);
  //food
  x := Player.get_x;
  y := Player.get_y;
  points := Food_list.check(x, y);
  if (points = -1) and (Player.get_lifes < 20) then
    Player.set_lifes(1)
  else if (Player.get_lifes <= 20) and (points <> -1) then
    Player.set_points(points);
end;

procedure Twindow.Level1Click(Sender: TObject);
begin
  if (not Levels_list.check_blocked(1)) then begin
    level.number := 1;
    load;
  end;
end;

procedure Twindow.Level2Click(Sender: TObject);
begin
  if (not Levels_list.check_blocked(2)) then begin
    level.number := 2;
    load;
  end;
end;

procedure Twindow.Level3Click(Sender: TObject);
begin
  if (not Levels_list.check_blocked(3)) then begin
    level.number := 3;
    load;
  end;
end;

procedure Twindow.Level4Click(Sender: TObject);
begin
  if (not Levels_list.check_blocked(4)) then begin
    level.number := 4;
    load;
  end;
end;

procedure Twindow.Level5Click(Sender: TObject);
begin
  if (not Levels_list.check_blocked(5)) then begin
    level.number := 5;
    load;
  end;
end;

procedure Twindow.Level6Click(Sender: TObject);
begin
  if (not Levels_list.check_blocked(6)) then begin
    level.number := 6;
    load;
  end;
end;

procedure Twindow.save;
begin
  Levels_list.save(Player.get_points, level.number);
end;

procedure Twindow.level_over_timerTimer(Sender: TObject);
var x, y: integer;
begin
  x := Player.get_x;
  y := Player.get_y;
  if (Player.enough_points) then begin
    if (Bg.check_next(x, y)) and (Level.number<=5) then
    begin
      keys_timer.Enabled :=false;
      if (not Player.check_hidden) then begin
       Player.Hide;
       save;
       inc(level.number);
       Levels_list.unblock(level.number);
       Application.ProcessMessages;
      end;
      sleep(3000);
      continue;
    end
    else if (Bg.check_next(x, y)) and (Level.number=6) then
    begin
      keys_timer.Enabled :=false;
      if (not Player.check_hidden) then begin
       Player.Hide;
       save;
       Application.ProcessMessages;
      end;
      sleep(3000);
      button_menu.Click;
    end;
  end;
end;

procedure Twindow.load;
begin
  button_menu.Enabled:=true;
  Level1.Visible:=false;
  Level2.Visible:=false;
  Level3.Visible:=false;
  Level4.Visible:=false;
  Level5.Visible:=false;
  Level6.Visible:=false;
  background.Canvas.Draw(0, 0, levels_list.LoadBg);
  Player.load;
  Food_list := TFood_list.Create(level.number);
  Hunters := THunters_list.Create(level.number);
  level.load(level.number);
  Bg := TBg.Create;
  Bg.load(level.number);
  save;
  keys_timer.Enabled := true;
  hunters_timer.Enabled := true;
  hunters_moving_timer.Enabled := true;
  game_timer.Enabled := true;
  level_over_timer.Enabled:=true;
end;

procedure Twindow.game_over;
begin
  Bg.Destroy;
  Player.Destroy;
  Hunters.Destroy;
  Level.Destroy;
  Food_list.Destroy;
end;

procedure Twindow.new_game;
begin
  levels_list.default;
  level.number:=1;
  save;
end;

procedure Twindow.continue;
var n: integer;
begin
  keys_timer.Enabled :=false;
  hunters_timer.Enabled := false;
  hunters_moving_timer.Enabled := false;
  game_timer.Enabled := false;
  level_over_timer.Enabled:=false;
  background.Canvas.Draw(0, 0, levels_list.LoadBg);
  n:=Level.number;
  Player.load;
  Level.Destroy;
    Level:=TLevel.Create(n);
    Level.load(n);
    Bg.Destroy;
    Bg:=TBg.Create;
    Bg.load(Level.number);
    Hunters.Destroy;
    Food_list.Destroy;
    Hunters := THunters_list.Create(Level.number);
    Food_list := TFood_list.Create(level.number);
    save;
    keys_timer.Enabled := true;
    hunters_timer.Enabled := true;
    hunters_moving_timer.Enabled := true;
    game_timer.Enabled := true;
    level_over_timer.Enabled:=true;
end;

procedure Twindow.next(var bm: TBitmap);
var x, y: integer;
begin
  x := Player.get_x;
  y := Player.get_y;
  if (not Bg.check_next(x, y))
      and (not Player.check_dead) then begin
    Level.information(bm);
    Player.information(bm)
  end
  else begin
    Player.set_state;
    Player.total_points(bm);
  end;
end;

end.
