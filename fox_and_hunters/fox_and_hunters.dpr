program fox_and_hunters;

uses
  Forms,
  main in 'main.pas' {window},
  level in 'level.pas',
  charact in 'charact.pas',
  animate in 'animate.pas',
  food in 'food.pas',
  bg in 'bg.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Twindow, window);
  Application.Run;
end.
