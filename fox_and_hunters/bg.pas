unit bg;

interface
uses animate, Graphics, SysUtils, Types;

type
  TExit = class
  public
    animation: TAnimation;
    position: TPoint;
    charwidth, charheight: integer;
    constructor create(level_number: integer);
  end;

  TBg = class
  private
    animation: TAnimation;
    exit: TExit;
  public
    procedure load(level_number: integer);
    procedure iter(var bm: TBitmap);
    function check_next(player_pos_x, player_pos_y: integer): boolean;
  end;

implementation

//Bg----------------------------------------------------------------------------

procedure TBg.load(level_number: integer);
begin
  animation := TAnimation.Create;
  animation.load('bg\bg\' + inttostr(level_number) + '\');
  exit:=TExit.Create(level_number);
end;

procedure TBg.iter(var bm: TBitmap);
begin
   bm.Canvas.Draw(0, 0, animation.current_frame);
   bm.Canvas.Draw(exit.position.X-exit.charwidth,
                  500-exit.position.Y-exit.charheight, exit.animation.current_frame);
end;

function TBg.check_next(player_pos_x, player_pos_y: integer):boolean;
begin
  if ((abs(player_pos_x - exit.position.X) < exit.charwidth) and
      (abs(player_pos_y - exit.position.Y) < exit.charheight)) then begin
        check_next := true;
      end
  else
    check_next:= false;
end;

//Exit--------------------------------------------------------------------------

constructor TExit.create(level_number: integer);
var f: textfile;
begin
  animation:=TAnimation.Create;
  animation.load('bg\exit\exit\');
  assignfile(f, 'bg\exit\'+inttostr(level_number)+'\exit.txt');
  reset(f);
  read(f, position.X);
  read(f, position.Y);
  closefile(f);
  charwidth:=animation.current_frame.Width div 2;
  charheight:=animation.current_frame.Height div 2;
end;


end.
