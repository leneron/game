unit level;

interface

uses Graphics, SysUtils, charact, food, animate;

type
  TLevel = class
  private
    procedure get_mask(var Mask: TMask; n: integer);
  public
    points: integer;
    unblocked: boolean;
    preview_blocked: TBitmap;
    preview_opened: TBitmap;
    levelwidth, levelheight: integer;
    number: integer;
    LevelImage: TAnimation;
    Mask: TMask;
    constructor create(n: integer);
    function get_preview: TBitmap;
    procedure load(n: integer);
    procedure information(var bm: TBitmap);
  end;

  TLevels_list = class
  private
    list: array [1 .. 6] of TLevel;
  public
    LoadBg: TBitmap;
    MenuBg: TBitmap;
    ControlBg: TBitmap;
    constructor create;
    procedure unblock(n: integer);
    procedure display(var bm: TBitmap);
    procedure save(level_points, n: integer);
    procedure default;
    function check_blocked(n: integer): boolean;
  end;

implementation

// Level------------------------------------------------------------------------

constructor TLevel.create(n: integer);
var
  s: string;
begin
  preview_blocked := TBitmap.create;
  preview_blocked.LoadFromFile('levels\levels\' + inttostr(n) + '\blocked.bmp');
  preview_opened := TBitmap.create;
  preview_opened.LoadFromFile('levels\levels\' + inttostr(n) + '\opened.bmp');
  number := n;
end;

procedure TLevel.load(n: integer);
var
  s: string;
begin
  s := 'bg\bg\' + inttostr(n) + '\';
  LevelImage := TAnimation.create;
  LevelImage.load(s);
  get_mask(Mask, n);
end;

procedure TLevel.get_mask(var Mask: TMask; n: integer);
var
  LevelMask: TBitmap;
  i, j: integer;
  s: string;
begin
  s := 'bg\mask\' + inttostr(n) + '.bmp';
  LevelMask := TBitmap.create;
  LevelMask.LoadFromFile(s);
  levelwidth := LevelMask.Width;
  levelheight := LevelMask.Height;
  fillchar(Mask, sizeof(Mask), true);
  for i := 0 to levelwidth - 1 do
    for j := 0 to levelheight - 1 do
    begin
      if LevelMask.Canvas.Pixels[i, j] = $FFFFFF then
        Mask[i, j] := true
      else
        Mask[i, j] := false;
    end;
end;

function TLevel.get_preview: TBitmap;
begin
  if unblocked = true then
    get_preview := preview_opened
  else
    get_preview := preview_blocked;
end;

procedure TLevel.information(var bm: TBitmap);
var
  s: string;
begin
  bm.Canvas.Brush.Style := bsClear;
  bm.Canvas.Font.Color := clWhite;
  bm.Canvas.Font.Size := 10;
  s := '�������: ' + inttostr(number);
  bm.Canvas.TextOut(15, 35, s);
end;

// Level_list--------------------------------------------------------------------

constructor TLevels_list.create;
var
  f: textfile;
  i: integer;
  s: byte;
begin
  LoadBg := TBitmap.create;
  LoadBg.LoadFromFile('bg\load\load.bmp');
  MenuBg := TBitmap.create;
  MenuBg.LoadFromFile('bg\menu\menu.bmp');
  ControlBg := TBitmap.create;
  ControlBg.LoadFromFile('bg\menu\control.bmp');
  assignfile(f, 'levels\saved.txt');
  reset(f);
  while (not seekeof(f)) do
  begin
    read(f, i);
    list[i] := TLevel.create(i);
    read(f, s);
    if s = 1 then
      list[i].unblocked := true
    else
      list[i].unblocked := false;
    read(f, list[i].points);
  end;
  closefile(f);
end;

procedure TLevels_list.unblock(n: integer);
begin
  list[n].unblocked := true;
end;

procedure TLevels_list.display(var bm: TBitmap);
var
  x, y, i: integer;
  s: string;
begin
  bm.Canvas.Brush.Style := bsClear;
  bm.Canvas.Font.Color := clWhite;
  bm.Canvas.Font.Size := 10;
  x := 97;
  y := 98;
  for i := 1 to 3 do
  begin
    bm.Canvas.draw(x, y, list[i].get_preview);
    s := '����: ' + inttostr(list[i].points);
    bm.Canvas.TextOut(x + 5, y + 70, s);
    x := x + 208;
  end;
  x := 97;
  y := 287;
  for i := 4 to 6 do
  begin
    bm.Canvas.draw(x, y, list[i].get_preview);
    s := '����: ' + inttostr(list[i].points);
    bm.Canvas.TextOut(x + 5, y + 70, s);
    x := x + 208;
  end;
end;

procedure TLevels_list.default;
var
  i: integer;
begin
  for i := 1 to length(list) do
  begin
    if i = 1 then
      list[i].unblocked := true
    else
      list[i].unblocked := false;
    list[i].points := 0;
  end;
end;

procedure TLevels_list.save(level_points, n: integer);
var
  i: integer;
  f: textfile;
  s: byte;
begin
  list[n].points := level_points;
  assignfile(f, 'levels\saved.txt');
  rewrite(f);
  for i := 1 to length(list) do
  begin
    if list[i].unblocked then
      s := 1
    else
      s := 0;
    write(f, list[i].number, ' ', s, ' ', list[i].points);
    writeln(f);
  end;
  closefile(f);
end;

function TLevels_list.check_blocked(n: integer): boolean;
begin
  if list[n].unblocked then
    check_blocked := false
  else
    check_blocked := true;
end;

end.
